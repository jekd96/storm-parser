package org.storm;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;
import org.storm.bolt.ParserContentBolt;
import org.storm.bolt.ParserPostBolt;
import org.storm.bolt.SearchBolt;
import org.storm.bolt.WriteFileBolt;
import org.storm.spout.LinkSpout;

/**
 * Created by Admin on 02.09.2016.
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("linkSpout",new LinkSpout("https://habrahabr.ru/flows/develop/all/page"), 2);
        builder.setBolt("openPageBolt", new SearchBolt(), 2).shuffleGrouping("linkSpout");
        builder.setBolt("parserPostBolt", new ParserPostBolt(), 2).shuffleGrouping("openPageBolt");
        builder.setBolt("parserContentBolt", new ParserContentBolt(), 2).shuffleGrouping("parserPostBolt");
        builder.setBolt("writeFileBolt", new WriteFileBolt(),2).shuffleGrouping("parserContentBolt");
        Config config = new Config();
        config.setDebug(false);
        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("user", config, builder.createTopology());
        Thread.sleep(10000*10);
        cluster.shutdown();
    }


}
