package org.storm.spout;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import java.util.Map;

/**
 * Created by Admin on 02.09.2016.
 */
public class LinkSpout extends BaseRichSpout  {

    private SpoutOutputCollector collector;
    private String link = "";
    private Long pageNumber = 1L;

    public LinkSpout(String link) {
        this.link = link;
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("link"));
    }

    public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        this.collector = spoutOutputCollector;
    }

    public void nextTuple() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {}
        collector.emit(new Values(link + pageNumber));
        pageNumber++;
    }

}
