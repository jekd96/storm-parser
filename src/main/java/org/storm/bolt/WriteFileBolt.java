package org.storm.bolt;

import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Admin on 05.09.2016.
 */
public class WriteFileBolt extends BaseBasicBolt {
    public void execute(Tuple input, BasicOutputCollector collector) {
        Map<String, String> news = (Map<String, String>) input.getValueByField("news");
        for (Map.Entry<String, String> entry : news.entrySet()) {
            File file = new File("C:/habra/"+ entry.getKey() +".txt");
            FileWriter fileWriter;
            try {
                file.createNewFile();
                fileWriter = new FileWriter(file);
                fileWriter.write(entry.getValue());
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }
}
