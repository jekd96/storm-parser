package org.storm.bolt;

import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 05.09.2016.
 */
public class ParserContentBolt extends BaseBasicBolt {

    public void execute(Tuple input, BasicOutputCollector collector) {
        Map<String, String> news = new HashMap<String, String>();
        List<HtmlDivision> htmlDivisionList = (List<HtmlDivision>) input.getValueByField("htmlDivisions");
        int i = 0;
        for (HtmlDivision htmlDivision : htmlDivisionList) {
            String link = "";
            if (htmlDivision.getFirstChild().getNextSibling() instanceof HtmlAnchor ) {
                link =  ((HtmlAnchor)  htmlDivision.getFirstChild().getNextSibling()).getHrefAttribute();
            }
            String content = link + "\n******************************************\n"
                    + htmlDivision.getParentNode().getTextContent();
            news.put(String.valueOf(i) +"__"+htmlDivision.getPage().hashCode(), content);
            i++;
        }
        collector.emit(new Values(news));
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("news"));
    }
}
