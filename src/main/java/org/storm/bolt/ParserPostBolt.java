package org.storm.bolt;

import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.List;

/**
 * Created by Admin on 05.09.2016.
 */
public class ParserPostBolt extends BaseBasicBolt {
    public void execute(Tuple input, BasicOutputCollector collector) {
        HtmlPage page = (HtmlPage) input.getValueByField("page");
        List<HtmlDivision> htmlDivisions = (List<HtmlDivision>) page.getByXPath("//div[@class='buttons']");
        collector.emit(new Values(htmlDivisions));
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("htmlDivisions"));
    }
}
